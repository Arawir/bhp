#include "main.h"


double expVal(std::string obsName, MPS *psi){
    for(int i=0; i<obsMPOs.size(); i++){
        if(obsNames[i] == obsName){
            return innerC(*psi,obsMPOs[i],*psi).real();
        }
    }
    return 0.0;
}

double calcVarN(MPS *psi){
    double tmp=0.0;
    for(int i=1; i<=L; i++){
        double tmp2 = ((double)N)/((double)L) - expVal("N_"+to_string(i), psi);
        tmp += tmp2*tmp2;
    }
    return sqrt(tmp);
}

void writeBasicStepInfo(int stepNum, int stateNum, MPS *psi){
    for(auto toWrite : obsToWrite){
        if(toWrite=="stepNum"){ std::cout << "stepNum=" << stepNum << "\t"; }
        if(toWrite=="stateNum"){ std::cout << "stateNum=" << stateNum << "\t"; }
        if(toWrite=="cutoff"){ std::cout << "cutoff=" << cutoff << "\t"; }
        if(toWrite=="maxDim"){ std::cout << "maxDim=" << maxdim << "\t"; }
        if(toWrite=="dim"){ std::cout << "dim=" << maxLinkDim(*psi) << "\t"; }
        if(toWrite=="rtime"){ std::cout << "rtime=" << (clock()-time0)/(double)CLOCKS_PER_SEC << "\t"; }
        if(toWrite=="varN"){ std::cout << "varN=" << calcVarN(psi) << "\t"; }
    }
}

void doSweepIfNeeded(std::vector<MPS> &tmpPsis, MPS *psi, Sweeps &sweep){
    doSweeps = (bool)loadParamFromFile("params.inp","doSweeps");
    if(doSweeps){
        if(tmpPsis.size()==0){
            dmrg((*psi),H,sweep,{"Quiet=",true,"Silent=",true});
        } else {
            dmrg(*psi,H,tmpPsis,sweep,{"Quiet=",true,"Silent=",true,"Weight=",8000.0*(double)tmpPsis.size()});
        }
    }
}

void loadModelParams(){
    time0 = clock();
    N = (int)loadParamFromFile("params.inp","N");
    d = (int)loadParamFromFile("params.inp","d");
    L = (int)loadParamFromFile("params.inp","L");
    g = loadParamFromFile("params.inp","g");
    P = loadParamFromFile("params.inp","P");
    V = loadParamFromFile("params.inp","V");

    sweepsInState = 0;
    continueCalc = (bool)loadParamFromFile("params.inp","continueCalc");


    Args::global().add("N", N);
    Args::global().add("d", d);
    Args::global().add("L", L);
    Args::global().add("g", g);
    Args::global().add("P", P);
    Args::global().add("V", V);
    Args::global().add("ConserveN", true);
}

void saveIfNeeded(){
    bool needSave = (bool)loadParamFromFile("params.inp","needSave");
    if(needSave){
        saveSession();
    }
}

int main(int argc, char *argv[])
{
    loadModelParams();

    int stepNum=0;

    openSession(continueCalc);


    auto sweep = Sweeps(1);
    sweep.mindim() = 10;
    sweep.niter() = 10;

    std::vector<MPS> tmpPsis;
    MPS *psi;


    while(1){
        sweepsPerState = (int)loadParamFromFile("params.inp","sweepsPerState");
        setSweep(sweep);
        setStateNumIfNeeded(tmpPsis,psi);

        doSweepIfNeeded(tmpPsis,psi,sweep);

        obsToWrite = loadParamFromFileVS("params.inp","Obs");
        writeBasicStepInfo(stepNum, stateNum, psi);
        writeMPOsValues(psi);
        std::cout << std::endl;

        stepNum++;
        saveIfNeeded();

    }

    return 0;
}
