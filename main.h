#include "interface.h"
#include "model.h"
#include "tdvp.h"
#include <fstream>
#include <ctime>

using namespace std;

////////////////////////////////////////////////////////////////////////
BasicSiteSet<BoseHubbardSite> sites;

MPS psiNull;
std::vector<MPS> psis;

MPO H;
std::vector<MPO> obsMPOs;
std::vector<std::string> obsNames;

std::vector<std::string> obsToWrite;

int N, L, d;
int stateNum;
double g, P, V;
bool continueCalc;
double cutoff;
int maxdim;
int sweepsPerState;
int sweepsInState;
bool doSweeps;

clock_t  time0;
/////////////////////////////////////////////////////////////////////////


void generateObservables(){
    auto ampo = AutoMPO(sites);
    double M = (double)L;

    for(int i=1; i<=sites.length(); i++){
        ampo += 1,"n",i;
    }

    obsNames.push_back("N");
    obsMPOs.push_back(toMPO(ampo));
    ampo.reset();
////////////
    for(int i=1; i<=sites.length(); i++){
        ampo += 1,"n2",i;
    }
    obsNames.push_back("N^2");
    obsMPOs.push_back(toMPO(ampo));
    ampo.reset();


////////////

    cpx tmp = im*M/4.0/M_PI;


    for(int j=1; j<L; j++){
        ampo += +tmp,"b",j,"bT",j+1;
        ampo += -tmp,"bT",j,"b",j+1;
    }
    ampo += +tmp,"b",L,"bT",1;
    ampo += -tmp,"bT",L,"b",1;

    obsNames.push_back("K");
    obsMPOs.push_back(toMPO(ampo));
    ampo.reset();
/////////////////
    cpx J = M*M/2.0;
    double U = g*M;

    for(int j=1; j<L; j++){
        ampo += -J,"b",j,"bT",j+1;
        ampo += -J,"bT",j,"b",j+1;
    }

    ampo += -J,"b",L,"bT",1;
    ampo += -J,"bT",L,"b",1;

    for(int j=1; j<=L; j++){
        ampo += U,"n(n-1)",j;
        ampo += 2.0*J,"n",j;
    }

    obsNames.push_back("E0");
    obsMPOs.push_back(toMPO(ampo));
    ampo.reset();


////////////
    for(int i=1; i<=sites.length(); i++){
        ampo += 1,"n",i;

        obsNames.push_back("N_"+to_string(i));
        obsMPOs.push_back(toMPO(ampo));
        ampo.reset();
    }
////////////
    for(int i=1; i<=sites.length(); i++){
        ampo += 1,"n2",i;

        obsNames.push_back("N^2_"+to_string(i));
        obsMPOs.push_back(toMPO(ampo));
        ampo.reset();
    }


////////////
    for(int i=1; i<=sites.length(); i++){
        auto n_0 = AutoMPO(sites);
        auto n_i = AutoMPO(sites);
        n_0 += 1,"n",(L+1)/4;
        if(i==(L+1)/4){
            n_i += 1,"n-1",i;
        } else {
            n_i += 1,"n",i;
        }

        obsNames.push_back("G2_L/4_"+to_string(i));
        obsMPOs.push_back( nmultMPO(toMPO(n_0), prime(toMPO(n_i))) );
    }

    ampo.reset();
////////////
    for(int i=1; i<=sites.length(); i++){
        auto n_0 = AutoMPO(sites);
        auto n_i = AutoMPO(sites);
        n_0 += 1,"n",(L+1)/2;
        if(i==(L+1)/2){
            n_i += 1,"n-1",i;
        } else {
            n_i += 1,"n",i;
        }

        obsNames.push_back("G2_L/2_"+to_string(i));
        obsMPOs.push_back( nmultMPO(toMPO(n_0), prime(toMPO(n_i))) );
    }

    ampo.reset();
}
///////////////////////////////////////////////////////////////////////
std::tuple<std::string,std::vector<std::string>> separateLine(std::string line)
{
    std::stringstream ss(line);
    std::istream_iterator<std::string> begin(ss);
    std::istream_iterator<std::string> end;
    std::vector<std::string> vstrings(begin, end);

    std::string name = vstrings[0];
    vstrings.erase(vstrings.begin());
    return std::make_tuple( name,vstrings );
}



double loadParamFromFile(std::string fileName, std::string paramName){
    std::ifstream file(fileName.c_str());
    std::string line;

    while (getline(file, line)){
        if(line!=""){
            auto [name,val] = separateLine(line);
            if(name==paramName){ file.close(); return atof(val[0].c_str()); }
        }
    }
    file.close();
    assert("Nieznany parametr");
    return 0.0;
}

std::string loadParamFromFileS(std::string fileName, std::string paramName){
    std::ifstream file(fileName.c_str());
    std::string line;

    while (std::getline(file, line)){
        if(line!=""){
            auto [name,val] = separateLine(line);
            if(name==paramName){ file.close(); return val[0]; }
        }
    }
    file.close();
    assert("Nieznany parametr");
    return "";
}

vector<string> loadParamFromFileVS(std::string fileName, std::string paramName){
    std::ifstream file(fileName.c_str());
    std::string line;

    while (std::getline(file, line)){
        if(line!=""){
            auto [name,val] = separateLine(line);
            if(name==paramName){ file.close(); return val; }
        }
    }
    file.close();
    assert("Nieznany parametr");
    return std::vector<string>{};
}




void setParamFromFile(std::string fileName, std::string paramName, double paramValue){
    std::fstream file(fileName.c_str(),std::ios::in);
    std::string line;

    std::vector<string> paramNames;
    std::vector<std::vector<string>> paramVals;

    while (std::getline(file, line)){
        if(line!=""){
            auto [name,val] = separateLine(line);
            paramNames.push_back(name);
            if(name==paramName){
                paramVals.push_back(std::vector<string>{to_string(paramValue)});
            } else{
                paramVals.push_back(val);
            }
        }
    }
    file.close();

    file.open(fileName.c_str(),std::ios::out);
    for(int i=0; i<paramNames.size(); i++){
        file << paramNames[i] << " ";
        for(int j=0; j<paramVals[i].size(); j++){
            file << paramVals[i][j] << " ";
        }
        file << std::endl;
    }
    file.close();
}

void newSession()
{
    seedRNG(1);

    sites = BoseHubbard(L);
    //psiNull = prepareInitState(sites);

    auto state = itensor::InitState(sites);
    for(int i=1;i<=L;i++){
        state.set(i,"0");
    }
    for(int i=1;i<=N;i++){
        state.set(i,"1");
    }
    psiNull = itensor::MPS(state);

    psis.clear();

    H = BHPHamiltonian(sites,L,g,P,V);
    generateObservables();
}

void saveSession(){
    writeToFile(format("PsiNull_N%d_g%f_L%d.exp",N,g,L),psiNull);
    for(int i=0; i<psis.size(); i++){
        writeToFile(format("Psi%d_N%d_g%f_L%d.exp",i,N,g,L),psis[i]);
    }


    std::fstream obsNamesFile;
    obsNamesFile.open("ObsNames.exp", std::ios::out);
    writeToFile(format("ObsH_N%d_g%f_L%d.exp",N,g,L),H);
    for(int i=0; i<obsMPOs.size(); i++){
        writeToFile(format("Obs%d_N%d_g%f_L%d.exp",i,N,g,L),obsMPOs[i]);
        obsNamesFile << obsNames[i] << " ";
    }
    obsNamesFile.close();

    writeToFile(format("Sites_N%d_g%f_L%d.exp",N,g,L),sites);
}



void loadSession(){
    seedRNG(1);

    readFromFile(format("Sites_N%d_g%f_L%d.exp",N,g,L),sites);
    readFromFile(format("PsiNull_N%d_g%f_L%d.exp",N,g,L),psiNull);
    readFromFile(format("ObsH_N%d_g%f_L%d.exp",N,g,L),H);

    int i=0;
    while(fileExists(format("Psi%d_N%d_g%f_L%d.exp",i,N,g,L))){
        psis.push_back( readFromFile<MPS>(format("Psi%d_N%d_g%f_L%d.exp",i,N,g,L)) );
        i++;
    }

    fstream obsNamesFile("ObsNames.exp", std::ios::in);
    i=0;
    while(fileExists(format("Obs%d_N%d_g%f_L%d.exp",i,N,g,L))){
        obsMPOs.push_back( readFromFile<MPO>(format("Obs%d_N%d_g%f_L%d.exp",i,N,g,L)) );
        std::string nextName;
        obsNamesFile >> nextName;
        obsNames.push_back(nextName);
        i++;
    }
    obsNamesFile.close();
}

void openSession(bool continueCalc){
    if(continueCalc){
        loadSession();
    } else {
        newSession();
    }
}



////////////////////////////////////////////




void setSweep(Sweeps &sweep){
    cutoff = loadParamFromFile("params.inp","cutoff");
    maxdim = (int)loadParamFromFile("params.inp","maxDim");
    sweep.maxdim() = maxdim;
    sweep.cutoff() = cutoff;
}

void updatePsisIfNeeded(){
    while(psis.size() < (stateNum+1)){
        psis.push_back(psiNull);
    }
}

bool stateNumChangedByUser(){
    if(stateNum!=(int)loadParamFromFile("params.inp","stateNum")) return true;
    return false;
}


void setStateNumIfNeeded(std::vector<MPS> &tmpPsis, MPS *&psi){
    sweepsInState++;

    if(stateNumChangedByUser()){
        sweepsInState=0;
    } else if(sweepsInState>sweepsPerState){
        sweepsInState=0;
        setParamFromFile("params.inp","stateNum",stateNum+1);
    }

    stateNum = (int)loadParamFromFile("params.inp","stateNum");
    updatePsisIfNeeded();

    if(stateNum != tmpPsis.size()){
        tmpPsis.clear();
        for(int i=0; i<stateNum; i++){
            tmpPsis.push_back(psis[i]);
        }    
    }
    psi = &psis[stateNum];
}

void writeExpVal(std::string nameWanted, MPS *psi){
    for(int i=0; i<obsMPOs.size(); i++){
        if(obsNames[i] == nameWanted){
            std::cout << obsNames[i] << "= " <<innerC(*psi,obsMPOs[i],*psi).real() << "\t";
        }
    }
}

void writeExpValNoName(std::string nameWanted, MPS *psi){
    for(int i=0; i<obsMPOs.size(); i++){
        if(obsNames[i] == nameWanted){
            std::cout << innerC(*psi,obsMPOs[i],*psi).real() << " ";
        }
    }
}

void writeExpVals(std::string nameWantedCore, MPS *psi){
    std::cout << nameWantedCore << "= ";
    for(int i=1; i<=L; i++){
        std::string nameWanted = nameWantedCore + to_string(i);
        writeExpValNoName(nameWanted,psi);
    }
    std::cout << "\t";
}

void writeMPOsValues(MPS* psi){
    for(auto nameWanted : obsToWrite){
        if(nameWanted=="E"){ std::cout << "E=" << innerC(*psi,H,*psi).real() << "\t"; }
        else if(nameWanted=="N_i"){ writeExpVals("N_",psi); }
        else if(nameWanted=="N^2_i"){ writeExpVals("N^2_",psi); }
        else if(nameWanted=="G2_L/4_i"){ writeExpVals("G2_L/4_",psi); }
        else if(nameWanted=="G2_L/2_i"){ writeExpVals("G2_L/2_",psi); }
        else { writeExpVal(nameWanted,psi); }
    }
}
