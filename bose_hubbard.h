#ifndef __ITENSOR_KONDO_HEISENBERG_H
#define __ITENSOR_KONDO_HEISENBERG_H
#include "itensor/mps/siteset.h"

namespace itensor {

    class BoseHubbardSite;
    using BoseHubbard = BasicSiteSet<BoseHubbardSite>;

    class BoseHubbardSite
    {
    private:
        Index s;
        int d;

    public:
        BoseHubbardSite() { }
        BoseHubbardSite(Index I) : s(I) { }

        BoseHubbardSite(Args const& args = Args::global())
        {
            auto ts = TagSet("Site,BH");
            if( args.defined("SiteNumber") )
                ts.addTags("n="+str(args.getInt("SiteNumber")));

            auto conserveN = args.getBool("ConserveN",false);
            d = args.getInt("d",3);


            if(conserveN){
                auto qints = Index::qnstorage(d);
                for(int n : range(d)) {
                    qints[n] = QNInt(QN({"N=",n}),1);
                }
                s = Index(std::move(qints),ts);
             } else {
                s = Index{ d,ts};
             }
        }

        Index index() const
        {
            return s;
        }

        IndexVal state(std::string const& state)
        {
            for(int n=0; n<d; n++){
                if(state == std::to_string(n)){ return s(n+1); }
            }
                Error("State " + state + " not recognized");

            return IndexVal{};
        }


        ITensor op(std::string const& opname, Args const& args) const
        {
            auto sP = prime(s);
            auto Op = ITensor{ dag(s),sP };

            if(opname == "b"){
                for(int i=1; i<d; i++){
                    Op.set(s(i+1),sP(i),std::sqrt((double)i));
                }
            }
            else if(opname == "b2"){
                for(int i=1; i<d-1; i++){
                    Op.set(s(i+2),sP(i),std::sqrt((double)(i*(i+1))));
                }
            }
            else if(opname == "bT"){
                for(int i=1; i<d; i++){
                    Op.set(s(i),sP(i+1),std::sqrt((double)i));
                }
            }
            else if(opname == "bT2"){
                for(int i=1; i<d-1; i++){
                    Op.set(s(i),sP(i+2),std::sqrt((double)(i*(i+1))));
                }
            }
            else if(opname == "n"){
                for(int i=1; i<=d; i++){
                    Op.set(s(i),sP(i),i-1);
                }
            }
            else if(opname == "n-1"){
                for(int i=1; i<=d; i++){
                    Op.set(s(i),sP(i),i-2);
                }
            }
            else if(opname == "n2"){
                for(int i=1; i<=d; i++){
                    Op.set(s(i),sP(i),(i-1)*(i-1));
                }
            }
            else if(opname == "l"){
                for(int i=1; i<d; i++){
                    Op.set(s(i),sP(i),i);
                }
            }
            else if(opname == "I"){
                for(int i=1; i<=d; i++){
                    Op.set(s(i),sP(i),1);
                }
            }
            else if(opname == "n(n-1)"){
                for(int i=1; i<=d; i++){
                    Op.set(s(i),sP(i),(i-1)*(i-2));
                }
            }

            else {
                Error("Operator \"" + opname + "\" name not recognized");
            }

            return Op;
        }
    };


} //namespace itensor

#endif
