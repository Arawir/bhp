#ifndef MODEL
#define MODEL

#include "itensor/all.h"
#include "bose_hubbard.h"
#include "interface.h"

using namespace itensor;

Sweeps prepareSweepClass()
{
    auto sweeps = Sweeps(Args::global().getInt("sweeps"));
    sweeps.maxdim() = Args::global().getInt("maxDim");
    sweeps.mindim() = Args::global().getInt("minDim");
    sweeps.cutoff() = Args::global().getReal("cutoff");
    sweeps.niter() = Args::global().getReal("niter");
    return sweeps;
}

MPO BHPHamiltonian(BoseHubbard &sites, int L, double g, double P, double V)
{
    double M = (double)L;
    cpx Jl = M*M/2.0-im*M*P/4.0/M_PI;
    cpx Jp = M*M/2.0+im*M*P/4.0/M_PI;
    double U = g*M/2.0;

    auto ampo = AutoMPO(sites);
    for(int j=1; j<L; j++){
        ampo += -Jp,"b",j,"bT",j+1;
        ampo += -Jl,"bT",j,"b",j+1;
    }

    ampo += -Jp,"b",L,"bT",1;
    ampo += -Jl,"bT",L,"b",1;

    for(int j=1; j<=L; j++){
        ampo += U,"n(n-1)",j;
    }

    ampo += V*M*M/2.0,"n",(L+1)/2;


    return toMPO(ampo);
}

std::tuple<BoseHubbard,MPS,MPO,Sweeps> prepareExpBasic()
{
    seedRNG(1);
    auto sites = BoseHubbard(getI("L"));
    auto psi = prepareInitState(sites);
    auto H = BHPHamiltonian(sites,getI("L"),getD("g"),getD("P"),getD("V"));
    auto sweeps = prepareSweepClass();

    return std::make_tuple( sites,psi,H,sweeps );
}

std::function<std::vector<std::string>()> generateInitState = [](){
    if(getI("N")>0){
        std::vector<std::string> states;
        int N = getI("N");
        int L = getI("L");
        double a = (double)N/(double)L;
        for(int i=0; i<L; i++){
            if( (double)N/(double)(L-i)>=a ){
                states.push_back("1");
                N--;
            } else {
                if((N==0)&&(i%2==0)){
                    states.insert(states.begin(),"0");
                } else {
                    states.push_back("0");
                }
            }
        }
        return states;
    }
    return parseInitState(Args::global().getString("state"));
};

void prepareObservables()
{
    ExpCon("N") = [](const BoseHubbard &sites){
        auto ampo = AutoMPO(sites);
        for(int i=1; i<=sites.length(); i++){
            ampo += 1,"n",i;
        }
        return toMPO(ampo);
    };
    ExpCon("K") = [](const BoseHubbard &sites){
        auto ampo = AutoMPO(sites);
        int L = getI("L");
        double M = (double)L;
        cpx tmp = im*M/4.0/M_PI;


        for(int j=1; j<L; j++){
            ampo += +tmp,"b",j,"bT",j+1;
            ampo += -tmp,"bT",j,"b",j+1;
        }
        ampo += +tmp,"b",L,"bT",1;
        ampo += -tmp,"bT",L,"b",1;

        return toMPO(ampo);
    };

    ExpCon("E0") = [](const BoseHubbard &sites){
        int L = getI("L");
        double M = (double)L;
        double g = getD("g");
        cpx J = M*M/2.0;
        double U = g*M;

        auto ampo = AutoMPO(sites);
        for(int j=1; j<L; j++){
            ampo += -J,"b",j,"bT",j+1;
            ampo += -J,"bT",j,"b",j+1;
        }

        ampo += -J,"b",L,"bT",1;
        ampo += -J,"bT",L,"b",1;

        for(int j=1; j<=L; j++){
            ampo += U,"n(n-1)",j;
            ampo += 2.0*J,"n",j;
        }
        return toMPO(ampo);
    };

    ExpCon("N1:L") = [](const BoseHubbard &sites){
        std::vector<MPO> out;
        int L = sites.length();

        for(int i=1; i<=sites.length(); i++){
            auto ampo = AutoMPO(sites);
            ampo += 1,"n",i;
            out.push_back( toMPO(ampo) );
        }

        return out;
    };

    ExpCon("G2_L/4:L") = [](const BoseHubbard &sites){
            std::vector<MPO> out;
            int L = sites.length();

            for(int i=1; i<=sites.length(); i++){
                auto n_0 = AutoMPO(sites);
                auto n_i = AutoMPO(sites);
                n_0 += 1,"n",(L+1)/4;
                if(i==(L+1)/4){
                    n_i += 1,"n-1",i;
                } else {
                    n_i += 1,"n",i;
                }

                out.push_back( nmultMPO(toMPO(n_0), prime(toMPO(n_i))) );
            }

            return out;
        };
        ExpCon("G2_L/2:L") = [](const BoseHubbard &sites){
            std::vector<MPO> out;
            int L = sites.length();

            for(int i=1; i<=sites.length(); i++){
                auto n_0 = AutoMPO(sites);
                auto n_i = AutoMPO(sites);
                n_0 += 1,"n",(L+1)/2;
                if(i==(L+1)/2){
                    n_i += 1,"n-1",i;
                } else {
                    n_i += 1,"n",i;
                }

                out.push_back( nmultMPO(toMPO(n_0), prime(toMPO(n_i))) );
            }

            return out;
        };

}


#endif // MODEL

